﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBar : MonoBehaviour
{public float MAX_MOVE =  2.0f;

    float  _displace = 0;

    [SerializeField]
    private float  _xSpeed =  0.02f;

    Vector3 _moveSpeed = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        _moveSpeed.y = _xSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += _moveSpeed;

        _displace +=  _moveSpeed.y;

        if (Mathf.Abs(_displace)  >  MAX_MOVE )
        {
            _displace = 0;
            _moveSpeed *= -1;
        }
       
        
    }
}
