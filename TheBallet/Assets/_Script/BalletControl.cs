﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalletControl : MonoBehaviour
{
   public float Speed = 500.0f ;

   void  FixedUpdate()
   {
       float _moveHorizontal = Input.GetAxis("Horizontal");
       float _moveVertical = Input.GetAxis("Vertical");

       Vector3 movement = new Vector3 (_moveHorizontal,0, _moveVertical);

       GetComponent<Rigidbody2D>().AddForce  (movement * Speed * Time.deltaTime);

   }
}
